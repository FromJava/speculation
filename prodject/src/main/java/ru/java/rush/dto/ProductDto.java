package ru.java.rush.dto;

import lombok.Data;

@Data
public class ProductDto {
    Integer id;
    String name;
    Integer purchasePrice;
    String packaging;//упаковка
    Integer salePrice;//цена реализации
}
