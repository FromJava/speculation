package ru.java.rush.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.java.rush.dto.ProductDto;
import ru.java.rush.entities.ProductEntity;
import ru.java.rush.repositories.ProductRepository;
import ru.java.rush.utils.MappingUtils;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository productRepository;
    private final MappingUtils mappingUtils;

    private final Integer margin = 5;
    private final String packaging = "Упаковано в лучшем виде";


    public void saveAll(List<ProductEntity> list) {
        productRepository.saveAll(list);
    }

    public ProductDto save(ProductDto dto) {
        ProductEntity entity = mappingUtils.mapToProductEntity(dto);
        productRepository.save(entity);
      
    }

    public void saveAllDto(List<ProductDto> list) {
        List<ProductEntity> collect =
                list.stream()
                        .map(mappingUtils::mapToProductEntity)
                        .collect(Collectors.toList());
        productRepository.saveAll(collect);
    }

    public List<ProductDto> findAll() {
        return productRepository.findAll().stream()
                .map(mappingUtils::mapToProductDto)
                .collect(Collectors.toList());
    }

    public ProductDto findById(Integer id) {
        return mappingUtils.mapToProductDto(
                productRepository.findById(id)
                        .orElse(new ProductEntity())
        );
    }

    // упаковываем товар
    public void pack(List<ProductDto> list) {
        list.forEach(productDto ->
                productDto.setPackaging(packaging)
        );
    }

    // делаем деньги
    public void makeMoney(List<ProductDto> list) {
        list.forEach(productDto ->
                productDto.setSalePrice(productDto.getPurchasePrice() * margin)
        );
    }

    //затираем цену оптовой покупки
    public void clear(List<ProductDto> list) {
        list.forEach(productDto ->
                productDto.setPurchasePrice(null)
        );
    }

}
